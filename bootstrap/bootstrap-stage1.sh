#!/bin/sh
#
# CrowdCI Bootstrap Stage 1
#
# Assumptions at this stage: We have network access.
#
### BEGIN INIT INFO
# Provides:          CrowdCI
# Required-Start:    $network
# Default-Start:     3
### END INIT INFO

# clone crowdci repo if that hasn't happend yet
until [ -f "/home/crowdci/crowdci/README" ]; do
    pushd /home/crowdci > /dev/null
    su crowdci -c "git clone git://anongit.kde.org/scratch/vkrause/crowdci"
    popd > /dev/null
    sleep 5
done

# Run bootstrap stage 2
/home/crowdci/crowdci/bootstrap/bootstrap-stage2.sh


