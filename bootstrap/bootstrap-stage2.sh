#!/bin/sh
#
# CrowdCI Bootstrap Stage 2
#
# This needs to run as root user!
# Assumptions at this stage: we have network access and a clone of our repo.
#

if [ "`whoami`" != "root" ]; then
    echo "Must run as root!"
    exit 1;
fi

pushd `dirname $0` > /dev/null

until [ -f "/home/crowdci/.crowdci.siteid" ]; do
    # install required packages
    packageList=`cat package-list-opensuse | tr '\n' ' '`
    zypper --non-interactive install $packageList

    # clone kdesrc-build
    pushd /home/crowdci > /dev/null
    su crowdci -c "git clone git://anongit.kde.org/kdesrc-build"
    popd > /dev/null

    # generate unique site id
    su crowdci -c "wget http://crowdci.akonadi-project.org/crowdci.php?siteid -O /home/crowdci/.crowdci.siteid"

    sleep 5
done

# setup cron job
ln -s /home/crowdci/crowdci/ci/crowdci-runner.cron /etc/cron.d/
chown root /etc/cron.d/crowdci-runner.cron

popd > /dev/null
