#!/bin/sh
#
# CrowdCI CI Runner 
#

if [ "`whoami`" != "crowdci" ]; then
    echo "Must run as crowdci user!"
    exit 1;
fi

if ! [ -f "/home/crowdci/.crowdci.siteid" ]; then
    echo "No site id found, incomplete bootstrap?"
    exit 1;
fi

pushd /home/crowdci 2>&1 > /dev/null

# check if there is already a CI runner active
if [ -f ".crowdci.lock" ]; then
    pid=`cat .crowdci.lock`
    if [ -f "/proc/$pid/cmdline" ]; then
        echo "CrowdCI runner already active, exiting."
        exit 1;
    fi
    echo "Ignoring stale lock file."
fi

echo $$ > ".crowdci.lock"

# update corwdci and kdesrc-build repos
pushd crowdci 2>&1 > /dev/null
git pull
popd 2>&1 > /dev/null

pushd kdesrc-build 2>&1 > /dev/null
git pull
popd 2>&1 > /dev/null

# get kdesrc-build config file
siteId=`cat .crowdci.siteid`
wget "http://crowdci.akonadi-project.org/crowdci.php?kdesrc-buildrc&siteid=$siteId" -O ~/.kdesrc-buildrc

# run kdesrc-build, full rebuild of KDE stuff, incremental for dependencies
kdesrc-build/kdesrc-build qt-copy
kdesrc-build/kdesrc-build kdesupport-git-no-cdash
kdesrc-build/kdesrc-build --refresh-build

rm -f ".crowdci.lock"

popd 2>&1 > /dev/null
