<?php
/*
 * CrowdCI configuration dispatcher
 */

header('Content-type: text/plain');

// send kdesrc-buildrc
// TODO: include site name in request
if ( isset($_REQUEST['kdesrc-buildrc']) ) {
    header('Content-Disposition: attachment; filename="kdesrc-buildrc"');

    # determine build type
    $buildTypes[0] = 'Release';
    $buildTypes[1] = 'MinSizeRel';
    $buildTypes[2] = 'RelWithDebInfo';
    $buildTypes[3] = 'Debug';
    $buildTypes[4] = 'Debugfull';

    $buildType = rand() % 5;

    $template = file_get_contents('kdesrc-buildrc.template');
    $template = str_ireplace('{% BUILDNAME %}', 'CrowdCI-' . $buildTypes[$buildType], $template);
    $template = str_ireplace('{% BUILDTYPE %}', $buildTypes[$buildType], $template);
    $template = str_ireplace('{% SITENAME %}', 'CrowdCI-' . $_REQUEST['siteid'], $template);
    echo $template;

// send unique site id
} else if ( isset($_REQUEST['siteid']) ) {
    $currentId = 0;
    if ( file_exists('crowdci.siteid' ) ) {
	    $currentId = file_get_contents( 'crowdci.siteid' );
    }
    $currentId = $currentId + 1;
    file_put_contents('crowdci.siteid', "$currentId");
    echo "$currentId";
}

?>
